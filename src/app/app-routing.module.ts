import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'lazy1', loadChildren: 'app/lazy1/lazy1.module#Lazy1Module'},
  {path: 'lazy2', loadChildren: 'app/lazy2/lazy2.module#Lazy2Module'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
