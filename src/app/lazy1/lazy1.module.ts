import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lazy1Component } from './lazy1/lazy1.component';
import { Lazy1RoutingModule } from './lazy1-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    Lazy1RoutingModule,
    SharedModule
  ],
  declarations: [Lazy1Component]
})
export class Lazy1Module { }
