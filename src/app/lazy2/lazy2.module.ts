import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lazy2Component } from './lazy2/lazy2.component';
import { Lazy2RoutingModule } from './lazy2-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    Lazy2RoutingModule,
    SharedModule
  ],
  declarations: [Lazy2Component]
})
export class Lazy2Module { }
