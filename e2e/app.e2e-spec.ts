import { RouterLazySharedPage } from './app.po';

describe('router-lazy-shared App', () => {
  let page: RouterLazySharedPage;

  beforeEach(() => {
    page = new RouterLazySharedPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
